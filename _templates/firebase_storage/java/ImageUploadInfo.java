package ch.fbi.tinderimmo;


public class ImageUploadInfo {
    public String imageName;
    public String imageURL;
    
    // empty constructor
    public ImageUploadInfo() {}

    // constructor
    public ImageUploadInfo(String name, String url) {
        this.imageName = name;
        this.imageURL= url;
    }
    
    // getters and setters
    public String getImageName() { return imageName; }
    public String getImageURL() { return imageURL; }
}
