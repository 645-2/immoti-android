package ch.fbi.tinderimmo.app.util;

public enum Status {
    DRAFT(0),
    PUBLICATED(1),
    DELETED(2),
    APPROVED(3),
    REJECTED(4),
    FORLATER(5);

    Status(int i)
    {
        this.id = i;
    }

    private int id;

    public int getId()
    {
        return id;
    }
}
