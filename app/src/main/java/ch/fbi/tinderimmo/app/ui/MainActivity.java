package ch.fbi.tinderimmo.app.ui;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.database.entity.AssetEntity;
import ch.fbi.tinderimmo.app.database.entity.CityEntity;
import ch.fbi.tinderimmo.app.database.entity.VoteEntity;
import ch.fbi.tinderimmo.app.util.DownloadImageTask;
import ch.fbi.tinderimmo.app.util.FlippView;
import ch.fbi.tinderimmo.app.util.MyGestureListener;
import ch.fbi.tinderimmo.app.util.OnSwipeListener;
import ch.fbi.tinderimmo.app.util.Vote;


public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = "Main";
    private TextView mHeaderEmailView;
    private Menu menuNav;
    private ViewFlipper modeFlipper;
    private Button buttonOwnerLogin;
    private List<CityEntity> mCities;
    private DrawerLayout drawer;
    private List<AssetEntity> allAssets, likedAssets;
    private List<VoteEntity> allVotesFromUID;
    private int currentImage;
    private AssetEntity currentAsset;
    private AutoCompleteTextView renterAssetsCity;
    private ImageView assetImage;

    private GestureDetector gestureDetector;

    private Button likeButton;
    private Button matchButton;

    private int swipeBottomCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        assetImage = findViewById(R.id.imageView_renterAsset_image);
        assetImage.setOnClickListener(v -> swapAssetImage());

        NavigationView navigationView = findViewById(R.id.mainNavView);
        // Header View
        View headerView = navigationView.getHeaderView(0);
        mHeaderEmailView = headerView.findViewById(R.id.mainSubtitleTextView);
        // Menu item
        menuNav = navigationView.getMenu();

        //buttons
        Button buttonOwnerViewAssets = findViewById(R.id.button_owner_viewassets);
        buttonOwnerViewAssets.setOnClickListener(v -> openViewAsset());

        buttonOwnerLogin = findViewById(R.id.button_owner_login);
        buttonOwnerLogin.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), LoginActivity.class)));

        likeButton = findViewById(R.id.like_button);
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putSerializable("likes", (Serializable) likedAssets);
                Intent intent = new Intent(getApplicationContext(), RenterLikedAssetsActivity.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        matchButton = findViewById(R.id.match_button);
        matchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putSerializable("likes", (Serializable) likedAssets);
                Intent intent = new Intent(getApplicationContext(), RenterMatchesActivity.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        Toolbar toolbar = findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);

        modeFlipper = findViewById(R.id.appBarMainModeFlipper);
        View renterView = findViewById(R.id.appBarMainModeRenter);
        View ownerView = findViewById(R.id.appBarMainModeOwner);

        modeFlipper.removeAllViews();
        modeFlipper.addView(renterView, FlippView.RENTER.getId());
        modeFlipper.addView(ownerView, FlippView.OWNER.getId());

        renterAssetsCity =  findViewById(R.id.textView_renterassets_city);

        ToggleButton mode = findViewById(R.id.appBarMainToggle);
        mode.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                Toast.makeText(MainActivity.this, R.string.main_modeowneractivated,
                        Toast.LENGTH_SHORT).show();
                modeFlipper.setDisplayedChild(FlippView.OWNER.getId());
            } else {
                Toast.makeText(MainActivity.this, R.string.main_moderenteractivated,
                        Toast.LENGTH_SHORT).show();
                modeFlipper.setDisplayedChild(FlippView.RENTER.getId());
            }
        });

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.main_navigationdraweropen, R.string.main_navigationdrawerclose);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        gestureDetector = new GestureDetector(MainActivity.this, new MyGestureListener(new OnSwipeListener() {
            @Override
            public void onSwipeTop() {
                voteAsset(Vote.PENDING);
            }

            @Override
            public void onSwipeBottom() {
                if(swipeBottomCounter == 9) {
                    Intent viewChat = new Intent(getApplicationContext(), ChatActivity.class);
                    viewChat.putExtra("chatFB", "all");
                    viewChat.putExtra("chatRoomName", "All chat room");
                    startActivity(viewChat);
                }else {
                    swipeBottomCounter++;
                }
            }

            @Override
            public void onSwipeRight() {
                voteAsset(Vote.LIKE);
            }

            @Override
            public void onSwipeLeft() {
                voteAsset(Vote.DISLIKE);
            }

        }));

        initRenterView();
        initFilterCities();
        initUserVotes();
    }

    // MB: votes methods
    public void voteAsset(Vote vote) {

        if(currentAsset != null){

            currentUser = mAuth.getCurrentUser();
            if(currentUser == null)
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            else{

                voteFeedback(vote);
                VoteEntity newVote = new VoteEntity(vote, currentUser.getEmail(), currentUser.getUid(),currentAsset.getFB_Key(),currentAsset.getOwnerUID(),currentAsset.getTitle(),currentAsset.getCity());
                newVote.setLastUpdate();

                //check if already voted on the asset
                boolean alreadyVoted = false;
                for (VoteEntity pastVote : allVotesFromUID){
                    if(pastVote.getAssetFbKey().equals(newVote.getAssetFbKey()) && newVote.getUserUID().equals(pastVote.getUserUID())){
                        alreadyVoted = true;
                        if(pastVote.getChoice() != newVote.getChoice()){
                            pastVote.setChoice(newVote.getChoice());
                            pastVote.updateValueAtNode("votes");
                        }
                    }
                }
                if(!alreadyVoted)
                    newVote.writeAtNode("votes");
            }
        }

    }

    private void voteFeedback(Vote vote){

        ImageView voteFeedbackView = new ImageView(this);

        switch(vote){
            case LIKE:
                voteFeedbackView.setImageResource(R.drawable.ic_like);
                break;
            case DISLIKE:
                voteFeedbackView.setImageResource(R.drawable.ic_dislike);
                break;
            default:
                voteFeedbackView.setImageResource(R.drawable.ic_pending);
                break;
        }
        // MB: variables for the swipes
        Toast voteFeedbackToast = new Toast(this);
        voteFeedbackToast.setView(voteFeedbackView);
        voteFeedbackToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        voteFeedbackToast.setDuration(Toast.LENGTH_SHORT);
        voteFeedbackToast.show();
    }

    private void swapAssetImage() {
        if(currentAsset != null){
            int amountImages = currentAsset.getImagesUrls().size();
            if(amountImages>1){
                currentImage++;
                if(currentImage+1 > amountImages)
                    currentImage = 0;

                String imageUrl = currentAsset.getImagesUrls().get(currentImage);
                new DownloadImageTask(assetImage)
                        .execute(imageUrl);
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    private void initFilterCities() {
        Query cities = fireBaseDB.getReference("cities")
                .orderByChild("name");

        cities.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                mCities =  new ArrayList<>();
                for (DataSnapshot citySnapshot: snapshot.getChildren()) {
                    CityEntity city = citySnapshot.getValue(CityEntity.class);
                    mCities.add(city);
                }

                String[] cities = new String[mCities.size()];
                for(int i = 0; i< mCities.size(); i++)
                    cities[i] = mCities.get(i).getName();

                ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, cities);
                renterAssetsCity.setAdapter(adapter);
                renterAssetsCity.setOnItemClickListener((parent, view, position, id) -> filterAssets());
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("TINDER_DEBUG : error with loading assets");
            }
        });
    }

    private void initRenterView() {
        Query query = fireBaseDB.getReference("assets");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allAssets = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    allAssets.add(snapshot.getValue(AssetEntity.class));
                }

                filterAssets();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("TINDER_DEBUG : error with loading assets");
            }
        });
    }

    private void filterAssets(){


        List<AssetEntity> filteredAssets = allAssets == null ? new ArrayList<>() : new ArrayList<>(allAssets);

        List<AssetEntity> pendingAssets = new ArrayList<>();
        List<AssetEntity> dislikedAssets = new ArrayList<>();
        likedAssets = new ArrayList<>();
        int matchesCounter = 0;

        String filterCity = renterAssetsCity.getText().toString().toLowerCase();
        if(!filterCity.equals("")){
            filteredAssets.removeIf(fa -> !fa.getCity().toLowerCase().equals(filterCity));
        }


        //filter with votes
        if(allVotesFromUID != null && allAssets != null){

            List<VoteEntity> filteredVotes = new ArrayList<>(allVotesFromUID);
            filteredVotes.sort((v1, v2) -> Long.compare(v2.getLastUpdate(), v1.getLastUpdate()));

            for(int a = 0; a< filteredAssets.size(); a++){
                for(int v = 0; v<filteredVotes.size(); v++){
                    //is asset voted?
                    if(filteredAssets.get(a).getFB_Key().equals(filteredVotes.get(v).getAssetFbKey())){
                        Vote choice = filteredVotes.get(v).getChoice();
                        switch (choice){
                            case DISLIKE:
                                dislikedAssets.add(filteredAssets.get(a));
                                filteredAssets.remove(a);
                                break;
                            case LIKE:
                                likedAssets.add(filteredAssets.get(a));
                                filteredAssets.remove(a);
                                break;
                            case PENDING:
                                pendingAssets.add(filteredAssets.get(a));
                                filteredAssets.remove(a);
                                break;
                        }

                        // MB: is it a match?
                        if (filteredVotes.get(v).getOwnerChoice() == Vote.MATCHED) {
                            matchesCounter++;
                        }

                        filteredVotes.remove(v);
                        a--;
                        if(a < 0)
                            break;
                    }
                }
            }

            //we put the pendings at the end (we ordered them by last vote)
            filteredAssets.addAll(pendingAssets);

        }

        if(currentUser != null){
            for(int a = 0; a< filteredAssets.size(); a++){
                if(filteredAssets.get(a).getOwnerUID().equals(currentUser.getUid())){
                    filteredAssets.remove(a);
                    a--;
                }
            }
        }

        //set first asset as displayed
        if(filteredAssets.size()>0)
            setCurrentAsset(filteredAssets.get(0));
        else
            setCurrentAsset(null);

        // MB: set the like button
        if (likedAssets.size() > 0) {
            String likes = getResources().getString(R.string.main_mylikestext) + " (" + likedAssets.size() + ")";
            likeButton.setText(likes);
            likeButton.setEnabled(true);
        } else {
            String likes = getResources().getString(R.string.main_mylikestext) + " (0)";
            likeButton.setText(likes);
            likeButton.setEnabled(false);
        }

        // MB: set the match button
        if (matchesCounter > 0) {
            String matches = getResources().getString(R.string.main_mymatchestext) + " (" + matchesCounter + ")";
            matchButton.setText(matches);
            matchButton.setEnabled(true);
        } else {
            String matches = getResources().getString(R.string.main_mymatchestext) + " (0)";
            matchButton.setText(matches);
            matchButton.setEnabled(false);
        }

        Log.d(TAG, "All assets :"+allAssets);
        Log.d(TAG, "Disliked assets :"+ dislikedAssets);
        Log.d(TAG, "Liked assets :"+likedAssets);
        Log.d(TAG, "Pending assets :"+ pendingAssets);
        Log.d(TAG, "Filtered assets :"+ filteredAssets);
    }

    private void initUserVotes() {

        Query query = fireBaseDB.getReference("votes");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                allVotesFromUID = new ArrayList<>();
                String uid = currentUser == null? "" : currentUser.getUid();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    VoteEntity vote = snapshot.getValue(VoteEntity.class);

                    if(vote.getUserUID().equals(uid))
                        allVotesFromUID.add(vote);
                }

                filterAssets();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("TINDER_DEBUG: error with loading assets");
            }
        });
    }

    private void setCurrentAsset(AssetEntity asset) {

        TextView title = findViewById(R.id.text_renterAsset_title);
        TextView m2 = findViewById(R.id.text_renterAsset_m2);
        TextView rooms = findViewById(R.id.text_renterAsset_nbrOfRooms);
        TextView city = findViewById(R.id.text_renterAsset_city);

        if(asset != null){
            currentAsset = asset;
            title.setText(asset.getTitle());
            m2.setText(asset.getM2() + getString(R.string.main_m2));
            rooms.setText(asset.getNbrOfRooms() + getString(R.string.main_pieces));
            city.setText(asset.getCity());
            if (asset.getImagesUrls().size() > 0) {
                currentImage = 0;
                String imageUrl = asset.getImagesUrls().get(0);
                new DownloadImageTask(assetImage)
                        .execute(imageUrl);
            } else {
                assetImage.setImageResource(R.drawable.no_image);
            }
        }
        else{
            currentAsset = null;
            title.setText(getString(R.string.main_noresult));
            m2.setText("");
            rooms.setText("");
            city.setText("");
            assetImage.setImageBitmap(null);
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        updateUI();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(MainActivity.this, "No setting for now",
                    Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void signOut() {
        mAuth.signOut();
        Toast.makeText(MainActivity.this, getString(R.string.main_signouttoast),
                Toast.LENGTH_SHORT).show();
        updateUI();
    }

    private void updateUI() {
        currentUser = mAuth.getCurrentUser();

        if (currentUser != null) {
            Log.d(TAG, "connected user");
            mHeaderEmailView.setText(currentUser.getEmail());

            menuNav.findItem(R.id.nav_login).setVisible(false);
            menuNav.findItem(R.id.nav_logout).setVisible(true);
            buttonOwnerLogin.setVisibility(View.INVISIBLE);

        } else {
            Log.d(TAG, "offline user");

            mHeaderEmailView.setText(R.string.main_notconnected);

            menuNav.findItem(R.id.nav_login).setVisible(true);
            menuNav.findItem(R.id.nav_logout).setVisible(false);
            buttonOwnerLogin.setVisibility(View.VISIBLE);
        }
        filterAssets();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_login) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        } else if (id == R.id.nav_logout) {
            signOut();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void openViewAsset(){
        //Check if connected
        currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            Toast.makeText(MainActivity.this, R.string.main_needtoconnect,
                    Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        }else{
            startActivity(new Intent(getApplicationContext(), OwnerAssetsActivity.class));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        updateUI();
        filterAssets();
    }
}
