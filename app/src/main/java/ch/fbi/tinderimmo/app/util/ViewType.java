package ch.fbi.tinderimmo.app.util;

public enum ViewType {
    Owner_Assets,
    Renter_Assets,
    Asset_Images,
    Matches,
    Renter_Assets_Liked,
    Asset_Likes,
    Messages
}