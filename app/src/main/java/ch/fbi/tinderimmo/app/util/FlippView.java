package ch.fbi.tinderimmo.app.util;

public enum FlippView {
    RENTER(0),
    OWNER(1);


    FlippView(int i)
    {
        this.id = i;
    }

    private int id;

    public int getId()
    {
        return id;
    }
}
