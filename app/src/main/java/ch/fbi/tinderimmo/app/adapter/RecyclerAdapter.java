package ch.fbi.tinderimmo.app.adapter;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.database.entity.AssetEntity;
import ch.fbi.tinderimmo.app.database.entity.MessageEntity;
import ch.fbi.tinderimmo.app.database.entity.VoteEntity;
import ch.fbi.tinderimmo.app.util.DownloadImageTask;
import ch.fbi.tinderimmo.app.util.RecyclerViewItemClickListener;
import ch.fbi.tinderimmo.app.util.ViewType;
import ch.fbi.tinderimmo.app.util.Vote;


public class RecyclerAdapter<T> extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private List<T> mData;
    private RecyclerViewItemClickListener mListener;
    private ViewType type;

    @NonNull
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //layout depend on object type
        LinearLayout lineInView = null;
        ArrayList<View> viewObjects = new ArrayList<>();

        //define objets on which we will bind values
        if (type == ViewType.Owner_Assets) {
            lineInView = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_owner_asset_line, parent, false);
            viewObjects.add(lineInView.findViewById(R.id.svAssetTitle));
            viewObjects.add(lineInView.findViewById(R.id.svAssetCity));
            viewObjects.add(lineInView.findViewById(R.id.imageButton_ownerassets_viewlikes));

        } else if(type == ViewType.Asset_Images){
            lineInView = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_owner_asset_image_line, parent, false);
            viewObjects.add(lineInView.findViewById(R.id.imageView_asset_images));

        } else if (type == ViewType.Renter_Assets_Liked) {
            lineInView = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_renter_liked_asset_line, parent, false);

            viewObjects.add(lineInView.findViewById(R.id.renterAssetTitle));
            viewObjects.add(lineInView.findViewById(R.id.renterAssetCity));
            viewObjects.add(lineInView.findViewById(R.id.likedAsset_background));
        } else if (type == ViewType.Matches) {
            lineInView = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_renter_matches, parent, false);

            viewObjects.add(lineInView.findViewById(R.id.matchAssetTitle));
        }else if(type == ViewType.Messages){
            lineInView = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_chat_message, parent, false);
            viewObjects.add(lineInView.findViewById(R.id.textviewt_message_message));
            viewObjects.add(lineInView.findViewById(R.id.textview_message_messenger));
            viewObjects.add(lineInView.findViewById(R.id.textview_message_time));
            viewObjects.add(lineInView.findViewById(R.id.linearlayout_message_content));
            viewObjects.add(lineInView.findViewById(R.id.linearlayout_message_bubbles));
        }
        else if (type == ViewType.Asset_Likes){
            lineInView = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_owner_asset_like, parent, false);

            viewObjects.add(lineInView.findViewById(R.id.text_like_email));
            viewObjects.add(lineInView.findViewById(R.id.imageButton_ownerassetlikes_accept));
            viewObjects.add(lineInView.findViewById(R.id.imageButton_ownerassetlikes_refuse));
            viewObjects.add(lineInView.findViewById(R.id.linearLayout_ownerassetlikes_line));
            viewObjects.add(lineInView.findViewById(R.id.text_like_status));
            viewObjects.add(lineInView.findViewById(R.id.imageButton_ownerassetlikes_openchat));
        }

        final RecyclerAdapter.ViewHolder viewholder = new RecyclerAdapter.ViewHolder(lineInView, viewObjects);

        //set listener on buttons
        if (type == ViewType.Owner_Assets){
            ImageView viewImages = lineInView.findViewById(R.id.imageButton_ownerassets_viewimages);
            viewImages.setOnClickListener(view -> mListener.onItemClick(viewholder.getAdapterPosition(),view.getId()));

            ImageButton viewLikes = lineInView.findViewById(R.id.imageButton_ownerassets_viewlikes);
            viewLikes.setOnClickListener(view -> mListener.onItemClick(viewholder.getAdapterPosition(),view.getId()));
        }
        else if(type == ViewType.Asset_Likes){
            ImageButton accept = lineInView.findViewById(R.id.imageButton_ownerassetlikes_accept);
            accept.setOnClickListener(view -> mListener.onItemClick(viewholder.getAdapterPosition(),view.getId()));

            ImageButton refuse = lineInView.findViewById(R.id.imageButton_ownerassetlikes_refuse);
            refuse.setOnClickListener(view -> mListener.onItemClick(viewholder.getAdapterPosition(),view.getId()));

            ImageButton openChat = lineInView.findViewById(R.id.imageButton_ownerassetlikes_openchat);
            openChat.setOnClickListener(view -> mListener.onItemClick(viewholder.getAdapterPosition(),view.getId()));
        } else if (type == ViewType.Matches) {
            ImageButton chat = lineInView.findViewById(R.id.icon_match);
            chat.setOnClickListener(view -> mListener.onItemClick(viewholder.getAdapterPosition(), view.getId()));
        }


        if (lineInView != null) {
            lineInView.setOnClickListener(view -> {
                mListener.onItemClick(viewholder.getAdapterPosition(),view.getId());
            });
            lineInView.setOnLongClickListener(view -> {
                mListener.onItemLongClick(viewholder.getAdapterPosition(),view.getId());
                return true;
            });
        }

        return viewholder;
    }

    public RecyclerAdapter(RecyclerViewItemClickListener listener, ViewType type) {
        this.type = type;
        mListener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.ViewHolder holder, int position) {
        T item = mData.get(position);

        if (type == ViewType.Owner_Assets){
            AssetEntity asset = (AssetEntity) item;
            ((TextView) holder.mListObjects.get(0)).setText(asset.getTitle());
            ((TextView) holder.mListObjects.get(1)).setText(asset.getCity());
            int nbrLike = asset.getAssetLikes().size();
            ImageButton likesButton = ((ImageButton)holder.mListObjects.get(2));
            if(nbrLike == 0){
                likesButton.setEnabled(false);
                likesButton.setImageResource(R.drawable.ic_like_button_grey);
            }
            else{
                likesButton.setEnabled(true);
                likesButton.setImageResource(R.drawable.ic_like_button);
            }
        } else if(type == ViewType.Renter_Assets_Liked){
            VoteEntity vote = (VoteEntity) item;
            LinearLayout layout = ((LinearLayout)holder.mListObjects.get(2));
            ((TextView) holder.mListObjects.get(0)).setText(vote.getAssetName());
            ((TextView) holder.mListObjects.get(1)).setText(vote.getAssetCity());

            if (vote.getOwnerChoice() == Vote.REFUSED) {
                layout.setBackgroundColor(Color.rgb(252, 184, 183));
            } else {
                layout.setBackgroundColor(Color.rgb(222, 254, 237));
            }
        } else if (type == ViewType.Matches) {
            VoteEntity vote = (VoteEntity) item;
            ((TextView) holder.mListObjects.get(0)).setText(vote.getAssetName());
        } else if (type == ViewType.Asset_Images){
            String imageUrl = item.toString();
            ImageView image = (ImageView)holder.mListObjects.get(0);
            new DownloadImageTask(image)
                    .execute(imageUrl);
        }
        else if (type == ViewType.Asset_Likes){
            VoteEntity vote = (VoteEntity) item;
            ((TextView) holder.mListObjects.get(0)).setText(vote.getEmail());
            ImageButton acceptButton = ((ImageButton)holder.mListObjects.get(1));
            ImageButton refuseButton = ((ImageButton)holder.mListObjects.get(2));
            LinearLayout likeLine = ((LinearLayout)holder.mListObjects.get(3));
            TextView likeStatus = ((TextView)holder.mListObjects.get(4));
            ImageButton openChat = ((ImageButton)holder.mListObjects.get(5));

            Vote ownerVote = vote.getOwnerChoice();
            switch(ownerVote){
                case MATCHED:
                    acceptButton.setVisibility(View.INVISIBLE);
                    refuseButton.setVisibility(View.INVISIBLE);
                    likeStatus.setVisibility(View.VISIBLE);
                    likeStatus.setText(ownerVote.toString());
                    openChat.setVisibility(View.VISIBLE);
                    likeLine.setBackgroundColor(0xFFFFD700);
                    break;
                case REFUSED:
                    acceptButton.setVisibility(View.INVISIBLE);
                    refuseButton.setVisibility(View.INVISIBLE);
                    likeStatus.setVisibility(View.VISIBLE);
                    openChat.setVisibility(View.INVISIBLE);
                    likeStatus.setText(ownerVote.toString());
                    likeLine.setBackgroundColor(0xFFC0C0C0);
                    break;
                    default:
                        openChat.setVisibility(View.INVISIBLE);
                        acceptButton.setVisibility(View.VISIBLE);
                        refuseButton.setVisibility(View.VISIBLE);
                        likeStatus.setVisibility(View.INVISIBLE);
            }
        }
        if(type == ViewType.Messages){
            MessageEntity message = (MessageEntity) item;
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            String dateString = formatter.format(new Date(message.getLastUpdate() * 1000));

            ((TextView) holder.mListObjects.get(0)).setText(message.getText());
            ((TextView) holder.mListObjects.get(2)).setText(dateString);

            if(message.isHisYour()) {
                ((TextView) holder.mListObjects.get(1)).setVisibility(View.GONE);
                ((LinearLayout) holder.mListObjects.get(3)).setGravity(Gravity.RIGHT);
                ((LinearLayout) holder.mListObjects.get(4)).setBackgroundResource(R.drawable.my_message);
            }else{
                ((TextView) holder.mListObjects.get(1)).setText(message.getAuthor());
                ((LinearLayout) holder.mListObjects.get(3)).setGravity(Gravity.LEFT);
                ((LinearLayout) holder.mListObjects.get(4)).setBackgroundResource(R.drawable.their_message);
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout mListItem;
        ArrayList<View> mListObjects;

        ViewHolder(LinearLayout listItem, ArrayList<View> listObjects){
            super(listItem);
            mListItem = listItem;
            mListObjects = listObjects;
        }
    }



    public void setData ( final List<T> data){
            if (mData == null) {
                mData = data;
                notifyItemRangeInserted(0, data.size());
            } else {
                DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                    @Override
                    public int getOldListSize() {
                        return mData.size();
                    }

                    @Override
                    public int getNewListSize() {
                        return data.size();
                    }

                    @Override
                    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                        if (mData instanceof AssetEntity) {
                            return ((AssetEntity) mData.get(oldItemPosition)).getTitle().equals(((AssetEntity) mData.get(newItemPosition)).getTitle());
                        }
                        return false;
                    }

                    @Override
                    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                        if (mData instanceof AssetEntity) {
                            AssetEntity newAsset = (AssetEntity) data.get(newItemPosition);
                            AssetEntity oldAsset = (AssetEntity) mData.get(oldItemPosition);
                            return Objects.equals(newAsset.getCity(), oldAsset.getCity())
                                    && Objects.equals(newAsset.getOwnerUID(), oldAsset.getOwnerUID())
                                    && Objects.equals(newAsset.getTitle(), oldAsset.getTitle());
                        }
                        return false;
                    }
                });
                mData = data;
                result.dispatchUpdatesTo(this);
            }
        }


    @Override
    public int getItemCount() {
        if (mData != null) {
            return mData.size();
        } else {
            return 0;
        }
    }
}
