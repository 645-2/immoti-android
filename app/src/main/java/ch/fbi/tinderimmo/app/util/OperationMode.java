package ch.fbi.tinderimmo.app.util;

public enum OperationMode {
    View,
    Edit,
    Delete,
    Save,
    GetAll
}
