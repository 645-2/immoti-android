package ch.fbi.tinderimmo.app.util;

public interface RecyclerViewItemClickListener {
    void onItemClick(int position, int viewId);
    void onItemLongClick(int position, int viewId);
}
