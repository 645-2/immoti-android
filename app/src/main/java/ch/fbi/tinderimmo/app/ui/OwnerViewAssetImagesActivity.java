package ch.fbi.tinderimmo.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.adapter.RecyclerAdapter;
import ch.fbi.tinderimmo.app.database.entity.AssetEntity;
import ch.fbi.tinderimmo.app.database.entity.CityEntity;
import ch.fbi.tinderimmo.app.util.FlippView;
import ch.fbi.tinderimmo.app.util.RecyclerViewItemClickListener;
import ch.fbi.tinderimmo.app.util.Status;
import ch.fbi.tinderimmo.app.util.ViewType;

public class OwnerViewAssetImagesActivity extends BaseActivity{

    private RecyclerView recyclerAssetImages;
    private RecyclerAdapter assetImagesAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.owner_view_asset_images_activity);

        String assetFB = getIntent().getStringExtra("assetFB");

        recyclerAssetImages = findViewById(R.id.recyclerview_owner_viewassetimages);
        Button addImage = findViewById(R.id.button_main_addassetimage);
        addImage.setOnClickListener(v -> {
            Intent addImages = new Intent(this,AddImagesActivity.class);
            addImages.putExtra("assetFB",assetFB);
            startActivity(addImages);
        });


        initOwnerAssetImages(assetFB);
    }


    private void initOwnerAssetImages(String assetFB){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerAssetImages.setLayoutManager(layoutManager);

        assetImagesAdapter = new RecyclerAdapter(new RecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position, int viewId) {

            }

            @Override
            public void onItemLongClick(int position,int viewId) {

            }

        }, ViewType.Asset_Images);


        Query currentAsset = fireBaseDB.getReference("assets")
                .orderByChild("fb_Key")
                .equalTo(assetFB);

        currentAsset.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                AssetEntity asset = null;
                for (DataSnapshot assetSnapshot: snapshot.getChildren()) {
                    asset = assetSnapshot.getValue(AssetEntity.class);
                }
                if(asset != null){
                    assetImagesAdapter.setData(asset.getImagesUrls());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        recyclerAssetImages.setAdapter(assetImagesAdapter);

    }

}
