package ch.fbi.tinderimmo.app.util;


import java.util.List;

import ch.fbi.tinderimmo.app.database.entity.BaseEntity;

public interface OnAsyncEventListener {
    void onSuccess(List<BaseEntity> result);
    void onFailure(Exception e);
}
