package ch.fbi.tinderimmo.app.database.entity;

public class MessageEntity extends BaseEntity{
    private String text;
    private String author;
    private boolean hisYour;

    public MessageEntity(){}

    public MessageEntity(String text, String author) {
        this.text = text;
        this.author = author;
        this.hisYour = false;
    }

    public String getText() {
        return text;
    }

    public String getAuthor() {
        return author;
    }

    public boolean isHisYour() {
        return hisYour;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setHisYour() {
        this.hisYour = true;
    }
}
