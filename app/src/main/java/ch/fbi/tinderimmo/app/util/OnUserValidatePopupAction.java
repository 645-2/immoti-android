package ch.fbi.tinderimmo.app.util;

public interface OnUserValidatePopupAction {
    void onValidate();
}
