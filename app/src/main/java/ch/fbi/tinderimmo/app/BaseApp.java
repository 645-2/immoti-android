package ch.fbi.tinderimmo.app;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.widget.Toast;

import ch.fbi.tinderimmo.app.database.DataGenerator;

public class BaseApp extends Application {


    public LayoutInflater getLayoutInflater(){
        return (LayoutInflater)getApplicationContext().getSystemService
                (getApplicationContext().LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DataGenerator dataGen = new DataGenerator(this);

    }
    public void displayShortToast(String text){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
