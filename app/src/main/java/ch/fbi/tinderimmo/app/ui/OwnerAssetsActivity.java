package ch.fbi.tinderimmo.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.adapter.RecyclerAdapter;
import ch.fbi.tinderimmo.app.database.entity.AssetEntity;
import ch.fbi.tinderimmo.app.database.entity.VoteEntity;
import ch.fbi.tinderimmo.app.util.RecyclerViewItemClickListener;
import ch.fbi.tinderimmo.app.util.ViewType;
import ch.fbi.tinderimmo.app.util.Vote;

public class OwnerAssetsActivity extends BaseActivity{

    private RecyclerView recyclerOwnerAssets;
    private List<AssetEntity> mOwnerAssets = new ArrayList<>();
    private List<VoteEntity> votesOnAssets = new ArrayList<>();
    private RecyclerAdapter assetsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.owner_assets_activity);


        // Initialize Firebase Auth
        currentUser = mAuth.getCurrentUser();
        if(currentUser == null)
            finish();

        //recyclers
        recyclerOwnerAssets =  findViewById(R.id.recyclerview_owner_viewassets);
        Button addAsset = findViewById(R.id.button_main_addasset);
        addAsset.setOnClickListener(v ->  startActivity(new Intent(getApplicationContext(), OwnerAddAssetActivity.class)));

        initOwnerAssets();

    }
    private void initVoteOnAssets() {
        //get all votes related to asset from the user
        Query query = fireBaseDB.getReference("votes")
                .orderByChild("ownerUID")
                .equalTo(mAuth.getCurrentUser().getUid());

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                votesOnAssets = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    VoteEntity vote = snapshot.getValue(VoteEntity.class);
                    votesOnAssets.add(vote);
                }

                assignLikesToAssets();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("TINDER_DEBUG: error with loading assets votes");
            }
        });
    }

    private void assignLikesToAssets(){
        if(mOwnerAssets.size() > 0 && votesOnAssets.size() > 0){
            List<VoteEntity> filteredVotes = new ArrayList<>(votesOnAssets);

            for(int a = 0; a < mOwnerAssets.size(); a++){
                AssetEntity asset = mOwnerAssets.get(a);
                for (int v = 0; v < filteredVotes.size(); v++){
                    VoteEntity vote = filteredVotes.get(v);
                    if(vote.getAssetFbKey().equals(asset.getFB_Key())){
                        if(vote.getChoice() == Vote.LIKE){
                            asset.addLike(vote);
                            mOwnerAssets.set(a,asset);
                        }
                        filteredVotes.remove(v);
                        v--;
                    }
                }
            }

            updateView();
        }
    }

    private void updateView(){
        if(mOwnerAssets.size() > 0 && assetsAdapter != null)
            assetsAdapter.setData(mOwnerAssets);
    }
    private void initOwnerAssets() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerOwnerAssets.setLayoutManager(layoutManager);

        assetsAdapter = new RecyclerAdapter(new RecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position, int viewId) {
                ImageButton viewImages = findViewById(R.id.imageButton_ownerassets_viewimages);
                ImageButton viewLikes = findViewById(R.id.imageButton_ownerassets_viewlikes);

                AssetEntity asset = mOwnerAssets.get(position);
                if (viewId == viewImages.getId()) {
                    Intent viewAsset = new Intent(getApplicationContext(), OwnerViewAssetImagesActivity.class);
                    viewAsset.putExtra("assetFB",asset.getFB_Key());
                    startActivity(viewAsset);
                }
                else if(viewId == viewLikes.getId()){
                    Intent viewAssetLikes = new Intent(getApplicationContext(), OwnerViewAssetLikesActivity.class);
                    viewAssetLikes.putExtra("assetFB",asset.getFB_Key());
                    startActivity(viewAssetLikes);
                }
            }

            @Override
            public void onItemLongClick(int position, int viewId) {

            }

        }, ViewType.Owner_Assets);

        String ownerUID = mAuth.getCurrentUser().getUid();
        Query ownerAssets = fireBaseDB.getReference("assets")
                .orderByChild("ownerUID")
                .equalTo(ownerUID);

        ownerAssets.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                mOwnerAssets = new ArrayList<>();
                for (DataSnapshot assetSnapshot : snapshot.getChildren()) {
                    AssetEntity asset = assetSnapshot.getValue(AssetEntity.class);
                    mOwnerAssets.add(asset);
                }
                initVoteOnAssets();
                updateView();

                if (assetsAdapter.getItemCount() == 0)
                    Toast.makeText(OwnerAssetsActivity.this, getString(R.string.ownerassets_nothingtodisplay),
                            Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("TINDER_DEBUG : error with loading assets");
            }
        });

        recyclerOwnerAssets.setAdapter(assetsAdapter);
    }



}
