package ch.fbi.tinderimmo.app.database.entity;

import ch.fbi.tinderimmo.app.util.Vote;

public class VoteEntity extends BaseEntity{

    private String userUID, assetFbKey, ownerUID, email, assetName, assetCity ,chatFB;
    private Vote ownerChoice, choice;
    private boolean alreadyNotified;

    public VoteEntity(){};
    public VoteEntity(Vote vote, String email, String UID, String assetFbKey, String ownerUID, String assetName, String assetCity){
        choice = vote;
        userUID = UID;
        this.assetFbKey = assetFbKey;
        this.ownerUID = ownerUID;
        this.email = email;
        this.assetName = assetName;
        this.assetCity = assetCity;
    }

    public String getUserUID() {
        return userUID;
    }

    public void setUserUID(String userUID) {
        this.userUID = userUID;
    }

    public String getAssetFbKey() {
        return assetFbKey;
    }

    public void setAssetFbKey(String assetFbKey) {
        this.assetFbKey = assetFbKey;
    }

    public Vote getChoice() {

        return choice == null? Vote.NONE : choice;
    }

    public void setChoice(Vote choice) {
        this.choice = choice;
    }

    public String getOwnerUID() {
        return ownerUID;
    }

    public void setOwnerUID(String ownerUID) {
        this.ownerUID = ownerUID;
    }

    public Vote getOwnerChoice() {
        return ownerChoice == null? Vote.NONE : ownerChoice;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setOwnerChoice(Vote ownerChoice) {
        this.ownerChoice = ownerChoice;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetCity() { return assetCity; }

    public void setAssetCity(String assetCity) {
        this.assetCity = assetCity;
	}
	
    public String getChatFB() {
        return chatFB;
    }

    public void setChatFB(String chatFB) {
        this.chatFB = chatFB;
    }

    public boolean isAlreadyNotified() {
        return alreadyNotified;
    }

    public void setAlreadyNotified(boolean alreadyNotified) {
        this.alreadyNotified = alreadyNotified;
    }
}
