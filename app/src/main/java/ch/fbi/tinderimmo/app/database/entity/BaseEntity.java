package ch.fbi.tinderimmo.app.database.entity;
import com.google.firebase.database.FirebaseDatabase;
import java.io.Serializable;


public abstract class BaseEntity
        implements Serializable {

    private static FirebaseDatabase fireBaseDB = FirebaseDatabase.getInstance();
    private String FB_Key;
    private long lastUpdate;

    public String getFB_Key(){return FB_Key;}
    public void setFB_Key(String fb_key){this.FB_Key = fb_key;}

    public static FirebaseDatabase getFbDb() {
        return fireBaseDB;
    }

    public void writeAtNode(String node){
        this.setFB_Key(fireBaseDB.getReference(node).push().getKey());
        fireBaseDB.getReference(node+"/" + this.getFB_Key()).setValue(this);
    }

    public void updateValueAtNode(String node){
        if(!getFB_Key().isEmpty())
            fireBaseDB.getReference(node+"/" + getFB_Key()).setValue(this);
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate() {
        this.lastUpdate = System.currentTimeMillis()/1000;
    }
}
