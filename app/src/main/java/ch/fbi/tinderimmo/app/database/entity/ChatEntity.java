package ch.fbi.tinderimmo.app.database.entity;

public class ChatEntity extends BaseEntity{
    private String ownerUID;
    private String renterUID;
    private String assetFBKey;
    private String assetName;

    public ChatEntity(){}

    public ChatEntity(String ownerUID, String renterUID, String assetFBKey, String assetName) {
        this.ownerUID = ownerUID;
        this.renterUID = renterUID;
        this.assetFBKey = assetFBKey;
        this.assetName = assetName;
    }

    public String getOwnerUID() {
        return ownerUID;
    }

    public String getRenterUID() {
        return renterUID;
    }

    public String getAssetFBKey() {
        return assetFBKey;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setOwnerUID(String ownerUID) {
        this.ownerUID = ownerUID;
    }

    public void setRenterUID(String renterUID) {
        this.renterUID = renterUID;
    }

    public void setAssetFBKey(String assetFBKey) {
        this.assetFBKey = assetFBKey;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }
}
