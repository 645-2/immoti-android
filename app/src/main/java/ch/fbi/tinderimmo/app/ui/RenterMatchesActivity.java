package ch.fbi.tinderimmo.app.ui;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;
import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.adapter.RecyclerAdapter;
import ch.fbi.tinderimmo.app.database.entity.AssetEntity;
import ch.fbi.tinderimmo.app.database.entity.VoteEntity;
import ch.fbi.tinderimmo.app.util.RecyclerViewItemClickListener;
import ch.fbi.tinderimmo.app.util.ViewType;
import ch.fbi.tinderimmo.app.util.Vote;


public class RenterMatchesActivity extends BaseActivity {

    // MB: variables
    private RecyclerView recycler;
    private RecyclerAdapter adapter;
    private List<VoteEntity> matches;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renter_matches);

        // MB: initialize variables
        ImageButton back = findViewById(R.id.imageButton_back);
        back.setOnClickListener(v -> {
            finish();
        });

        // MB: retrieve the list of liked assets
        Bundle b = getIntent().getExtras();
        List<AssetEntity> likedAssets = (ArrayList<AssetEntity>) b.getSerializable("likes");

        // MB: recycler
        recycler = findViewById(R.id.recyclerview_matches);

        initMatches(likedAssets);
    }

    private void initMatches(List<AssetEntity> assets) {
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this);
        recycler.setLayoutManager(layout);

        adapter = new RecyclerAdapter(new RecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position, int viewId) {
                ImageButton chat = findViewById(R.id.icon_match);

                VoteEntity vote = matches.get(position);
                if (viewId == chat.getId()) {
                    Intent viewChat = new Intent(getApplicationContext(), ChatActivity.class);
                    viewChat.putExtra("chatFB", vote.getChatFB());
                    viewChat.putExtra("chatRoomName", "Chat " + vote.getAssetName());
                    startActivity(viewChat);
                }
            }

            @Override
            public void onItemLongClick(int position, int viewId) {
            }
        }, ViewType.Matches);

        // MB: retrieve the list of matches
        Query query = fireBaseDB.getReference("votes")
                .orderByChild("userUID")
                .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                matches = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    VoteEntity vote = snapshot.getValue(VoteEntity.class);

                    if (vote.getOwnerChoice() == Vote.MATCHED) {
                        matches.add(vote);
                    }
                }

                adapter.setData(matches);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("TINDER_DEBUG : error with loading matches");
            }
        });

        recycler.setAdapter(adapter);
    }
}
