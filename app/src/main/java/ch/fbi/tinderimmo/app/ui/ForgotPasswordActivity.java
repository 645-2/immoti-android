package ch.fbi.tinderimmo.app.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import ch.fbi.tinderimmo.app.R;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "ForgotPassword";

    private TextView mStatusTextView;
    private EditText mEmailField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        // Views
        mStatusTextView = findViewById(R.id.status);
        mEmailField = findViewById(R.id.fieldEmail);

        // Buttons
        findViewById(R.id.forgotPasswordResetButton).setOnClickListener(this);
        findViewById(R.id.forgotPasswordBackButton).setOnClickListener(this);
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError(getText(R.string.forgotpassword_required));
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        return valid;
    }

    public void sendPasswordReset(String email) {
        Log.d(TAG, "sendPasswordReset:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        FirebaseAuth auth = FirebaseAuth.getInstance();

        auth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Email sent.");

                    mStatusTextView.setText(R.string.forgotpassword_statusok);

                    hideProgressDialog();
                }else{
                    mStatusTextView.setTextColor(Color.RED);
                    mStatusTextView.setText(R.string.forgotpassword_statuserror);

                    hideProgressDialog();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();

        if (i == R.id.forgotPasswordResetButton) {
            sendPasswordReset(mEmailField.getText().toString());
        } else if (i == R.id.forgotPasswordBackButton) {
             Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }
}
