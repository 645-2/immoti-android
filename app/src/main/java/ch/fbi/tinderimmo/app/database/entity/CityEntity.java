package ch.fbi.tinderimmo.app.database.entity;

public class CityEntity extends BaseEntity{

    private String postalCode;
    private String name;

    public CityEntity(String cp, String name) {
        this.postalCode = cp;
        this.name = name;
    }

    public CityEntity(){};
    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
