package ch.fbi.tinderimmo.app.database.entity;
import java.util.ArrayList;
import java.util.List;
import ch.fbi.tinderimmo.app.util.Status;
import ch.fbi.tinderimmo.app.util.Vote;


public class AssetEntity extends BaseEntity {

    private String title;
    private Status status;
    private String ownerUID;
    private String city;
    private ArrayList<String> imagesUrls = new ArrayList<>();
    private int nbrOfRooms;
    private int m2;
    private transient ArrayList<VoteEntity> assetLikes = new ArrayList<>();

    public int getNbrOfRooms() {
        return nbrOfRooms;
    }
    public void setNbrOfRooms(int nbrOfRooms) {
        this.nbrOfRooms = nbrOfRooms;
    }

    public void addLike(VoteEntity vote){
        assetLikes.add(vote);
    }
    public int getM2() {
        return m2;
    }
    public void setM2(int m2) {
        this.m2 = m2;
    }

    public String getOwnerUID() {
        return ownerUID;
    }
    public void setOwnerUID(String ownerUID) {
        this.ownerUID = ownerUID;
    }

    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public List<String> getImagesUrls() {
        return imagesUrls;
    }
    public void setImagesUrls(ArrayList<String> imagesUrls) {
        this.imagesUrls = imagesUrls;
    }

    public ArrayList<VoteEntity> getAssetLikes() {
        return assetLikes;
    }

    public void setAssetLikes(ArrayList<VoteEntity> assetLikes) {
        this.assetLikes = assetLikes;
    }
}
