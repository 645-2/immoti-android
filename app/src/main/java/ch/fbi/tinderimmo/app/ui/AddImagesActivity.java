package ch.fbi.tinderimmo.app.ui;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.IOException;

import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.database.entity.AssetEntity;

public class AddImagesActivity extends BaseActivity {

    private FirebaseDatabase fireBaseDB = FirebaseDatabase.getInstance();
    private static final String TAG = "Login";

    // repository path (name in firebase)
    String Storage_Path = "AssetImages/";

    // elements
    Button ChooseButton, UploadButton;
    ImageView SelectImage;
    Uri FilePathUri;
    ProgressDialog progressDialog ;
    String assetFB;
    // references
    StorageReference storageReference;

    // code onActivityResult()
    int Image_Request_Code = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_images);

        Intent asset = getIntent();
        assetFB = asset.getStringExtra("assetFB");


        // link references
        storageReference = FirebaseStorage.getInstance().getReference();

        // link elements
        ChooseButton = findViewById(R.id.mainImagesChooseImageButton);
        UploadButton = findViewById(R.id.mainImagesUploadImageButton);
        SelectImage = findViewById(R.id.mainImagesImageView);
        progressDialog = new ProgressDialog(AddImagesActivity.this);

        // choose an assetImage
        ChooseButton.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);

            startActivityForResult(Intent.createChooser(intent, getString(R.string.mainimages_selectimage)), Image_Request_Code);
        });

        // upload an assetImage
        UploadButton.setOnClickListener(view -> UploadImageFileToFirebaseStorage());
        // Initialize Firebase Auth
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    // onActivityResult action
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {
            FilePathUri = data.getData();

            try {
                // transform selected assetImage into ImageView
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), FilePathUri);
                SelectImage.setImageBitmap(bitmap);

                // change choose button above text
                ChooseButton.setText(getString(R.string.mainimages_imageselected));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // method to get the file extension
    public String GetFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;
    }

    // method to upload an assetImage on storage
    public void UploadImageFileToFirebaseStorage() {

        if (FilePathUri != null) {

            // progress dialog
            progressDialog.setTitle(getString(R.string.mainimages_uploading));
            progressDialog.show();

            // new storage reference
            StorageReference storageReference2nd = storageReference.child(Storage_Path + System.currentTimeMillis() + "." + GetFileExtension(FilePathUri));
            storageReference2nd.putFile(FilePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            // upload status of progress dialog and toast
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), getString(R.string.mainimages_uploadsuccess), Toast.LENGTH_LONG).show();

                            insertImageInFireBase(taskSnapshot.getDownloadUrl().toString());

                        }
                    })

                    // and on failure
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {

                            // update status and show an error
                            progressDialog.dismiss();
                            Toast.makeText(AddImagesActivity.this, getResources().getString(R.string.error_cloudnotavailable), Toast.LENGTH_LONG).show();
                            finish();
                        }
                    })

                    // and on progress
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.setTitle(getString(R.string.mainimages_uploading));
                        }
                    });
        } else {
            Toast.makeText(AddImagesActivity.this, getString(R.string.mainimages_selectimageordescription), Toast.LENGTH_LONG).show();
        }
    }

    private void insertImageInFireBase(String url) {
        Query currentAsset = fireBaseDB.getReference("assets")
                .orderByChild("fb_Key")
                .equalTo(assetFB);

        currentAsset.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                AssetEntity asset = null;
                for (DataSnapshot assetSnapshot: snapshot.getChildren()) {
                    asset = assetSnapshot.getValue(AssetEntity.class);
                }
                if(asset != null){
                    asset.getImagesUrls().add(url);

                    asset.setLastUpdate();
                    fireBaseDB.getReference("assets/"+asset.getFB_Key()).setValue(asset);
                    finish();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}