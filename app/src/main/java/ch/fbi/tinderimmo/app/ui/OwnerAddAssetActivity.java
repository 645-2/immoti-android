package ch.fbi.tinderimmo.app.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.database.entity.AssetEntity;
import ch.fbi.tinderimmo.app.database.entity.CityEntity;
import ch.fbi.tinderimmo.app.util.Status;

public class OwnerAddAssetActivity extends BaseActivity{

    private FirebaseAuth mAuth;
    private List<CityEntity> mCities;
    private EditText ownerAssetsTitle,ownerAssetsRooms, ownerAssetsM2;
    private AutoCompleteTextView ownerAssetsCity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.owner_add_asset_activity);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        ownerAssetsTitle = findViewById(R.id.textView_ownerassets_title);
        ownerAssetsCity =  findViewById(R.id.textView_ownerassets_city);
        ownerAssetsRooms = findViewById(R.id.textView_ownerassets_rooms);
        ownerAssetsM2 = findViewById(R.id.textView_ownerassets_m2);

        Button addAsset = findViewById(R.id.button_ownerassets_add);
        addAsset.setOnClickListener(v -> createAsset());

        initAddAssets();
    }

    private void initAddAssets() {

        Query cities = fireBaseDB.getReference("cities")
                .orderByChild("name");

        cities.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                mCities =  new ArrayList<>();
                for (DataSnapshot citySnapshot: snapshot.getChildren()) {
                    CityEntity city = citySnapshot.getValue(CityEntity.class);
                    mCities.add(city);
                }

                String[] cities = new String[mCities.size()];
                for(int i = 0; i< mCities.size(); i++)
                    cities[i] = mCities.get(i).getName();

                ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, cities);
                ownerAssetsCity.setAdapter(adapter);

                ownerAssetsCity.setOnFocusChangeListener((view, b) -> {
                    if(!b) {
                        String cityEntered = ownerAssetsCity.getText().toString();
                        for (String cityStr: cities) {
                            if(cityStr.toLowerCase().equals(cityEntered.toLowerCase())){
                                ownerAssetsCity.setText(cityStr);
                                return;
                            }
                        }

                        ownerAssetsCity.setText("");
                        Toast.makeText(OwnerAddAssetActivity.this, R.string.owneraddassets_invalidcity,
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("TINDER_DEBUG : error with loading assets");
            }
        });
    }


    private boolean checkFormAddAsset() {
        boolean valid = true;
        //title
        String title = ownerAssetsTitle.getText().toString();
        if (TextUtils.isEmpty(title)){
            ownerAssetsTitle.setError(getResources().getString(R.string.owneraddassets_invalidtitle));
            valid = false;
        } else {
            ownerAssetsTitle.setError(null);
        }

        //m2
        int m2 = 0;
        try{
            m2 = Integer.parseInt(ownerAssetsM2.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if(m2 <= 0) {
            ownerAssetsM2.setError(getResources().getString(R.string.owneraddassets_invalidm2));
            valid = false;
        } else {
            ownerAssetsM2.setError(null);
        }

        //rooms
        int rooms = 0;
        try{
            rooms = Integer.parseInt(ownerAssetsRooms.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if(rooms <= 0 || rooms > 30) {
            ownerAssetsRooms.setError(getResources().getString(R.string.owneraddassets_invalidrooms));
            valid = false;
        } else {
            ownerAssetsM2.setError(null);
        }

        //title
        String city = ownerAssetsCity.getText().toString();
        if (TextUtils.isEmpty(city)){
            ownerAssetsCity.setError(getResources().getString(R.string.owneraddassets_invalidcity));
            valid = false;
        } else {
            ownerAssetsCity.setError(null);
        }

        return valid;
    }

    private void createAsset(){
        if(checkFormAddAsset()){

            AssetEntity newAsset = new AssetEntity();
            newAsset.setTitle(ownerAssetsTitle.getText().toString());
            newAsset.setCity(ownerAssetsCity.getText().toString());
            newAsset.setM2(Integer.parseInt(ownerAssetsM2.getText().toString()));
            newAsset.setNbrOfRooms(Integer.parseInt(ownerAssetsRooms.getText().toString()));
            newAsset.setStatus(Status.DRAFT);
            String uid = mAuth.getCurrentUser().getUid();
            newAsset.setOwnerUID(uid);

            newAsset.setFB_Key(fireBaseDB.getReference("assets/").push().getKey());
            fireBaseDB.getReference("assets/"+newAsset.getFB_Key()).setValue(newAsset);

            finish();
        }
    }

}
