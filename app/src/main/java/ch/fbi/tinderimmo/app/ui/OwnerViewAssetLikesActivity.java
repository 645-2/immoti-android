package ch.fbi.tinderimmo.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.adapter.RecyclerAdapter;
import ch.fbi.tinderimmo.app.database.entity.ChatEntity;
import ch.fbi.tinderimmo.app.database.entity.VoteEntity;
import ch.fbi.tinderimmo.app.util.RecyclerViewItemClickListener;
import ch.fbi.tinderimmo.app.util.ViewType;
import ch.fbi.tinderimmo.app.util.Vote;

public class OwnerViewAssetLikesActivity extends BaseActivity{

    private RecyclerView recyclerAssetLikes;
    private RecyclerAdapter assetLikesAdapter;
    private List<VoteEntity> likes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.owner_view_asset_likes_activity);

        String assetFB = getIntent().getStringExtra("assetFB");

        recyclerAssetLikes = findViewById(R.id.recyclerview_owner_viewassetlikes);

        initOwnerAssetLikes(assetFB);
    }

    private void initOwnerAssetLikes(String assetFB){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerAssetLikes.setLayoutManager(layoutManager);

        assetLikesAdapter = new RecyclerAdapter(new RecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position, int viewId) {
                ImageButton refuseButton = findViewById(R.id.imageButton_ownerassetlikes_refuse);
                ImageButton acceptButton = findViewById(R.id.imageButton_ownerassetlikes_accept);
                ImageButton openChat = findViewById(R.id.imageButton_ownerassetlikes_openchat);

                VoteEntity likeVote = likes.get(position);
                if(viewId == refuseButton.getId()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.ownerviewassetlikes_refuse)+likeVote.getEmail(), Toast.LENGTH_SHORT).show();
                    likeVote.setOwnerChoice(Vote.REFUSED);
                    likeVote.updateValueAtNode("votes");
                }
                else if(viewId == acceptButton.getId()){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.ownerviewassetlikes_accept)+likeVote.getEmail(), Toast.LENGTH_SHORT).show();

                    ChatEntity chat = new ChatEntity(likeVote.getOwnerUID(), likeVote.getUserUID(), likeVote.getAssetFbKey(), likeVote.getAssetName());
                    chat.writeAtNode("chats");

                    likeVote.setOwnerChoice(Vote.MATCHED);
                    likeVote.setChatFB(chat.getFB_Key());
                    likeVote.updateValueAtNode("votes");
                }
                else if(viewId == openChat.getId()){
                    Intent viewChat = new Intent(getApplicationContext(), ChatActivity.class);
                    viewChat.putExtra("chatFB", likeVote.getChatFB());
                    viewChat.putExtra("chatRoomName", "Chat " + likeVote.getAssetName() + " - " + likeVote.getEmail());
                    startActivity(viewChat);
                }
            }

            @Override
            public void onItemLongClick(int position,int viewId) {

            }

        }, ViewType.Asset_Likes);


        Query currentAsset = fireBaseDB.getReference("votes")
                .orderByChild("assetFbKey")
                .equalTo(assetFB);

        currentAsset.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                likes = new ArrayList<>();
                for (DataSnapshot voteSnapshot: snapshot.getChildren()) {
                    VoteEntity vote = voteSnapshot.getValue(VoteEntity.class);
                    if(vote != null && vote.getChoice() == Vote.LIKE){
                        likes.add(vote);
                    }
                }
                assetLikesAdapter.setData(likes);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        recyclerAssetLikes.setAdapter(assetLikesAdapter);

    }

}
