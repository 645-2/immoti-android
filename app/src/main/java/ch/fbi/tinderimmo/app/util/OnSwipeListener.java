package ch.fbi.tinderimmo.app.util;


import android.view.MotionEvent;
import android.widget.Toast;

import java.util.List;

import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.database.entity.BaseEntity;

public interface OnSwipeListener {
    void onSwipeTop();
    void onSwipeBottom();
    void onSwipeLeft();
    void onSwipeRight() ;
}
