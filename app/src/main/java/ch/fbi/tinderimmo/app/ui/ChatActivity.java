package ch.fbi.tinderimmo.app.ui;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.adapter.RecyclerAdapter;
import ch.fbi.tinderimmo.app.database.entity.ChatEntity;
import ch.fbi.tinderimmo.app.database.entity.MessageEntity;
import ch.fbi.tinderimmo.app.util.RecyclerViewItemClickListener;
import ch.fbi.tinderimmo.app.util.ViewType;

public class ChatActivity extends BaseActivity {

    private static final String TAG = "MESSAGE";

    private RecyclerView recyclerMessages;
    private List<MessageEntity> mMessages;
    private RecyclerAdapter messagesAdapter;
    private String chatFBKey;
    private EditText mTextField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        currentUser = mAuth.getCurrentUser();
        if(currentUser == null)
            finish();

        chatFBKey = getIntent().getStringExtra("chatFB");
        String chatRoomName = getIntent().getStringExtra("chatRoomName");

        mTextField = findViewById(R.id.edittext_chat_message);

        recyclerMessages = findViewById(R.id.recyclerview_chat_messagelist);
        Button sendButton = findViewById(R.id.button_chat_send);
        sendButton.setOnClickListener(v ->  sendMessage());

        setTitle(chatRoomName);
        initMessages();
    }

    // Load messages in a recycler listner
    public void initMessages(){
        mMessages = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerMessages.setLayoutManager(layoutManager);

        messagesAdapter = new RecyclerAdapter(new RecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position, int viewId) { }

            @Override
            public void onItemLongClick(int position, int viewId) { }
        }, ViewType.Messages);

        Query messages = fireBaseDB.getReference("chats/"+ chatFBKey +"/messages");

        messages.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mMessages = new ArrayList<>();

                for (DataSnapshot assetSnapshot : snapshot.getChildren()) {
                    MessageEntity message = assetSnapshot.getValue(MessageEntity.class);
                    if(message.getAuthor().equals(mAuth.getCurrentUser().getEmail()))
                        message.setHisYour();

                    mMessages.add(message);
                }
                messagesAdapter.setData(mMessages);
                recyclerMessages.scrollToPosition(mMessages.size() - 1);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "error with loading messages");
            }
        });

        recyclerMessages.setAdapter(messagesAdapter);
    }

    public void sendMessage(){
        String textMessage = mTextField.getText().toString();

        if(textMessage.isEmpty())
            return;

        // Easter egg for totem
        if(textMessage.equals("fbi totem")){
            hideKeyboard(this.getCurrentFocus());

            new CountDownTimer(2000, 1000) {
                public void onTick(long millisUntilFinished) {
                    findViewById(R.id.imageview_chat_totem).setVisibility(View.VISIBLE);
                }

                public void onFinish() {
                    findViewById(R.id.imageview_chat_totem).setVisibility(View.INVISIBLE);
                }
            }.start();
        }

        // create a message
        MessageEntity message = new MessageEntity();
        message.setText(textMessage);
        message.setAuthor(mAuth.getCurrentUser().getEmail());
        message.setLastUpdate();

        // reset text field
        mTextField.setText("");

        // push to firebase
        String key = fireBaseDB.getReference("chats/"+ chatFBKey +"/messages/").push().getKey();
        message.setFB_Key(key);
        fireBaseDB.getReference("chats/"+ chatFBKey +"/messages/"+key).setValue(message);
    }
}
