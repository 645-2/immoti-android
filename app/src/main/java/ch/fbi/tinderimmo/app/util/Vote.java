package ch.fbi.tinderimmo.app.util;

public enum Vote {
    PENDING,
    LIKE,
    DISLIKE,
    MATCHED,
    REFUSED,
    NONE
}
