package ch.fbi.tinderimmo.app.ui;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import ch.fbi.tinderimmo.app.R;
import ch.fbi.tinderimmo.app.database.entity.VoteEntity;
import ch.fbi.tinderimmo.app.util.Vote;

public class BaseActivity extends AppCompatActivity {

    @VisibleForTesting
    protected ProgressDialog mProgressDialog;
    protected static FirebaseDatabase fireBaseDB = FirebaseDatabase.getInstance();
    protected FirebaseUser currentUser;
    protected FirebaseAuth mAuth;
    private long startTImestamp;

    // Source : https://www.tutorialspoint.com/how-to-create-a-notification-with-notificationcompat-builder-in-android
    public void notificationDialog(String title, String text){
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "tinderImmo";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);
            // Configure the notification channel.
            notificationChannel.setDescription("Tinder Immo Channel");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("Tinder Immog")
                .setContentTitle(title)
                .setContentText(text);
        notificationManager.notify(1, notificationBuilder.build());
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.all_loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void hideKeyboard(View view) {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgressDialog();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart(){
        super.onStart();
        startTImestamp = System.currentTimeMillis()/1000;
        initLikeFollow();
    }

    private void initLikeFollow() {
        //we listen to new like to inform owner
        Query assetLikes = fireBaseDB.getReference("votes")
                .orderByChild("choice")
                .equalTo(Vote.LIKE.toString());

        assetLikes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if(currentUser != null){

                    for (DataSnapshot voteSnapshot: snapshot.getChildren()) {
                        VoteEntity vote = voteSnapshot.getValue(VoteEntity.class);
                        if(vote != null && vote.getOwnerChoice() == Vote.NONE
                                && vote.getLastUpdate() > startTImestamp && vote.getOwnerUID().equals(currentUser.getUid()) && !vote.isAlreadyNotified()){

                            String title = getResources().getString(R.string.base_newlikenotification);
                            String text = getResources().getString(R.string.base_notificationlikemessage,vote.getEmail(),vote.getAssetName());

                            notificationDialog(title, text);
                            vote.setAlreadyNotified(true);
                            vote.updateValueAtNode("votes");
                        }
                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
