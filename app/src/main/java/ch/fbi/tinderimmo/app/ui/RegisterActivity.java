package ch.fbi.tinderimmo.app.ui;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;

import ch.fbi.tinderimmo.app.R;


public class RegisterActivity extends BaseActivity {

    // MB: declare elements
    private static final String TAG = "Register";
    private EditText email;
    private EditText password;
    private EditText confirmPassword;
    private CheckBox gdpr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // MB: link elements
        email = findViewById(R.id.registerEmail);
        password = findViewById(R.id.registerPassword);
        confirmPassword = findViewById(R.id.registerPasswordRepeat);

        findViewById(R.id.registerButton).setOnClickListener(v -> createAccount(email.getText().toString(), password.getText().toString()));
        findViewById(R.id.registerLogin).setOnClickListener(v -> {
                    startActivity(new Intent(this, LoginActivity.class));
                }
        );

        // MB: add checkbox for gdpr with a link
        gdpr = findViewById(R.id.registerGDPR);
        gdpr.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    // MB: create account method
    public void createAccount(String email, String password) {
        Log.d(TAG, "createAccount: " + email);

        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // MB: sign in user
                            Log.d(TAG, "createUserWithEmail: success");
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            Toast.makeText(RegisterActivity.this, R.string.register_success,
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.w(TAG, "createUserWithEmail: failure", task.getException());

                            // MB: display an error message (test the type of exception)
                            switch(((FirebaseAuthException)task.getException()).getErrorCode()) {
                                case "ERROR_WEAK_PASSWORD":
                                    Toast.makeText(RegisterActivity.this, R.string.register_weakpassword,
                                            Toast.LENGTH_SHORT).show();
                                    break;
                                case "ERROR_EMAIL_ALREADY_IN_USE":
                                    Toast.makeText(RegisterActivity.this, R.string.register_emailalreadyuse,
                                            Toast.LENGTH_SHORT).show();
                                    break;
                                default:
                                    Toast.makeText(RegisterActivity.this, R.string.register_error,
                                            Toast.LENGTH_SHORT).show();
                            }
                        }

                        hideProgressDialog();
                    }
                });
    }

    // MB: validation of the form
    private boolean validateForm() {
        boolean valid = true;

        // MB: email check
        String address = email.getText().toString();
        if (TextUtils.isEmpty(address) || !Patterns.EMAIL_ADDRESS.matcher(address).matches()) {
            email.setError(getResources().getString(R.string.register_emailinvalid));
            valid = false;
        } else {
            email.setError(null);
        }

        // MB: first password check
        String pass1 = password.getText().toString();
        if (TextUtils.isEmpty(pass1)) {
            password.setError(getResources().getString(R.string.register_passwordmissing));
            valid = false;
        } else {
            password.setError(null);
        }

        // MB: second password check
        String pass2 = confirmPassword.getText().toString();
        if (TextUtils.isEmpty(pass2)) {
            confirmPassword.setError(getResources().getString(R.string.register_mailconfirmationmissing));
            valid = false;
        } else {
            confirmPassword.setError(null);
        }

        // MB: passwords are the same
        if (!pass1.equals(pass2)) {
            confirmPassword.setError(getResources().getString(R.string.register_differentpasswords));
            valid = false;
        } else {
            confirmPassword.setError(null);
        }

        // MB: GDPR accepted
        if (!gdpr.isChecked()) {
            gdpr.setError(getResources().getString(R.string.register_gdprnotaccepted));
            valid = false;
        } else {
            gdpr.setError(null);
        }

        return valid;
    }

}
