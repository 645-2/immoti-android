package ch.fbi.tinderimmo.app.database;

import android.support.annotation.NonNull;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import ch.fbi.tinderimmo.app.BaseApp;
import ch.fbi.tinderimmo.app.database.entity.BaseEntity;
import ch.fbi.tinderimmo.app.database.entity.CityEntity;
import ch.fbi.tinderimmo.app.database.entity.AssetEntity;

/**
 * Generates dummy data
 */
public class DataGenerator {

    private final String TAG = "DataGenerator";
    private final BaseApp app;

    private FirebaseDatabase fireBaseDB = FirebaseDatabase.getInstance();

    private List<AssetEntity> assets;
    private List<CityEntity> cities;

    public DataGenerator(BaseApp app) {
        initFireBaseData();
        this.app = app;
    }

    private void initFireBaseData() {
        fireBaseDB.getReference().child("parameters/resetDB").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                boolean resetDB = true;
                if (snapshot.getValue() != null)
                    resetDB = !snapshot.getValue().equals("No");

                if (resetDB) {

                    //erase data from DB
                    fireBaseDB.getReference("cities").removeValue();
                    fireBaseDB.getReference("parameters/resetDB").setValue("No");

                    // populate cities from /swiss_locality.csv
                    cities = readCSV("swiss_locality");

                    for (CityEntity city : cities) {
                        city.writeAtNode("cities");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("PA_DEBUG : error init DB");
            }

        });

    }

    // Reader for CSV
    private ArrayList readCSV(String fileName) {
        ArrayList<BaseEntity> entities = new ArrayList<>();
        BufferedReader br;
        InputStream ins;
        try {

            ins = app.getResources().openRawResource(app.getResources().getIdentifier(fileName, "raw", app.getPackageName()));

            br = new BufferedReader(new InputStreamReader(ins, StandardCharsets.UTF_8));
            String line;
            int count = 0;

            while ((line = br.readLine()) != null && !line.isEmpty()){
                String[] fields = line.split(";");

                if (fileName.equals("swiss_locality")) {

                    String name = fields[0];
                    String cp = fields[1];
                    CityEntity city = new CityEntity(cp, name);
                    entities.add(city);

                }
                count++;

            }
            br.close();

        } catch (IOException e) {
            System.out.println("VB_DEBUG: " + e.toString());
        }

        return entities;

    }

}